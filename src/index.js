import Resolver from '@forge/resolver';

const resolver = new Resolver();

resolver.define('storeData', (req) => {
  console.log(req.payload);
});

export const handler = resolver.getDefinitions();
